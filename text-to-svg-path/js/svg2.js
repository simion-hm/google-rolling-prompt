const slowDuration = 1;

const textEase = CustomEase.create("custom", "M0,0,C0.084,0.61,0.132,0.811,0.19,0.874,0.354,1.0,0.374,1,1,1");
// const scrollEase = Power4.easeOut;

// const scrollEase = CustomEase.create("custom", "M0,0,C0.074,0,0.212,0.108,0.286,0.178,0.362,0.25,0.47,0.32,0.498,0.502,0.544,0.674,0.574,0.697,0.648,0.784,0.73,0.88,0.914,1,1,1");
// const scrollEase =  CustomEase.create("custom", "M0,0,C0.074,0,0.234,0.05,0.302,0.13,0.369,0.209,0.47,0.32,0.498,0.502,0.544,0.674,0.616,0.806,0.7,0.886,0.791,0.973,0.914,1,1,1");
let scrollEase = Sine.easeInOut;
// const scrollEase = Power0.easeNone;

const growEase = Power4.easeOut;
const shrinkEase = Power4.easeOut;

const h = 86.6;
const initialFontSize = "22.6px";
const grownFontSize = "38.5px";
const grownLineHeight = "48px";
const grownCornerRadius = "27px";
const normalCornerRadius = "23px";

const promptTextColor = "#797979";
const promptWidth = 508;
const promptHeight = 238;

const inBetweenPromptDistance = 16;
const stepSize = h + inBetweenPromptDistance;

let tail;
let sequences;
let playSequence = 1;

let questions;
let subSequences;

function loadJSON(callback) {
  let xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', '../sequences.json', false);
  xobj.onreadystatechange = function () {
    if (xobj.readyState === 4 && xobj.status === 200) {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}

const fadeColors = {
  text: ["#999999", "#BBBBBB", "#CCCCCC", "#E6E6E6"],
  background: ["#F7F7F7", "#FAFAFA", "#FBFBFB", "#FDFDFD",]
};

let container = document.getElementById("listContainer");
let timeline = new TimelineMax();
timeline.smoothChildTiming = true;

const questionChildren = [];
const questionWidth = [];
const svgId = "sid-";
const textId = "tid-";
let twoLiners;

loadJSON((response) => {
  let actual_JSON = JSON.parse(response);
  sequences = actual_JSON.sequences;
  twoLiners = actual_JSON.twoLiners;
  subSequences = actual_JSON.subSequences;

  questions = sequences[playSequence].reverse();

  createList(questions);
});

function createList(list) {
  for (let i = 0; i < questions.length; i++) {
    let svg = document.createElement('svg');
    let text = document.createElement('text');
    let tspan = document.createElement('tspan');

    // tspan.setAttribute('x', '0');
    text.setAttribute('id', textId + i);
    // text.setAttribute('y', (h/2).toString());
    text.setAttribute('alignment-baseline', 'central');
    tspan.setAttribute('text-anchor', 'middle');

    // svg.setAttribute('height', h.toString());
    svg.setAttribute('id', svgId + i);
    svg.setAttribute('class', "bubblePosition");

    tspan.innerHTML = list[i];

    text.appendChild(tspan);
    svg.appendChild(text);

    container.appendChild(svg);
  }
  tail = questions.length;
  console.log("length: " + tail);

}

function createAnimations(step, stepDuration, scrollingEase) {
  const growNo = tail - (step + 5);
  const shrinkNo = tail - (step + 4);
  let l = timeline.getLabelsArray();
  let moveTime = (step === 0) ? 0 : l[l.length - 1].time;

  updateAll();
  timeline.addLabel("label" + step);

  function updateAll() {
    rollAll();
  }

  function rollAll() {
    if (step === 0)
      timeline.staggerFrom(".bubblePosition", stepDuration,
        {
          ease: scrollEase,
          y: 0,
        },
        0);
    if (tail - (8 + step)) {
      timeline.staggerTo(".bubblePosition", stepDuration,
        {
          ease: scrollEase,
          y: stepSize * step,
          autoRound: false,
        },
        0, moveTime);
    }
  }
}

function initialize() {
  for (let i = 0; i < tail; i++) {
    const svg = document.getElementById(svgId + i);
    timeline.set(svg,
      {
        top: i * stepSize
      },
      0);
  }
  timeline.clear();
}

initialize();

function playOnClick() {
  for (let i = 0; i < 2; i++) {
    createAnimations(i, slowDuration, scrollEase);
  }
  timeline.pause();

  let play = false;
  document.addEventListener("click", () => {
    if (timeline.paused)
      timeline.play();
    else
      timeline.pause();
    play = !play;
  });
}

// playOnClick();

function question(index) {
  if (index > 0 && index < questions.length) {
    return questions[index];
  } else
    console.log("no question at index: " + index);
}

function grownText(index) {
  let questionText = question(index);
  if ((Object.keys(twoLiners)).includes(questionText))
    return twoLineCenteredSvgText(questionText, twoLiners[questionText]);
  return questionText;
}

function twoLineCenteredSvgText(text, position) {
  let spaces = [];
  let heyGoogle = "Hey Google, ";

  for (let i = 0; i < text.length; i++) {
    if (text[i] === " ") {
      spaces.push(i);
    }
  }

  const firstLine = heyGoogle + firstLetterLowerCase(text.slice(0, spaces[position]));
  const secondLine = text.slice(spaces[position]);

  return "<text transform=\"translate(287,110)\">"
    + " <tspan id=\"svgLine1\" x=\"0\" text-anchor=\"middle\">"
    + firstLine
    + "</tspan>"
    + " <tspan id=\"svgLine2\" x=\"0\" text-anchor=\"middle\" dy=\"48\">"
    + secondLine
    + "</tspan>" +
    "</text>";
}

function firstLetterLowerCase(text) {
  let firstLetter = text.charAt(0).toLowerCase();
  return firstLetter + text.slice(1);
}