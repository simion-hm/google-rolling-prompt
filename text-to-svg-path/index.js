const TextToSVG = require('text-to-svg');
const getBounds = require('svg-path-bounds');
let svgson = require('svgson');
const fs = require('fs');



const textToSVG = TextToSVG.loadSync("fonts/GoogleSans-Regular.ttf");
const optionsSmall = {x: 0, y: 0, fontSize: 22.6, anchor: 'top'};
const optionsOneLine = {x: 0, y: 0, fontSize: 38.5, anchor: 'top'};

let contents = fs.readFileSync("../sequences.json");
let jsonContents = JSON.parse(contents);

let q = {};

function firstLetterLowerCase(text) {
  let firstLetter = text.charAt(0).toLowerCase();
  return firstLetter + text.slice(1);
}

["1-thursday",
  "2-thursday",
  "3-thursday"].forEach((sequenceNo)=>{
  // console.log(jsonContents.sequences[sequenceNo]);
  (jsonContents.sequences[sequenceNo]).forEach((item, index) => {
    svgson(textToSVG.getSVG(item, optionsOneLine), {}, (bigLineResult) => {
      svgson(textToSVG.getSVG(item, optionsSmall), {}, (initialLineResult) => {

        let initialTestPath = textToSVG.getSVG(item);

        let twoLinePaths = item;

        if ((Object.keys(jsonContents.breakPoints)).includes(item))
          twoLinePaths = twoLinePath(item, jsonContents.breakPoints[item]);

        q[item] = {
          "svg": textToSVG.getSVG(item, optionsSmall),
          "initial": initialLineResult,
          "oneline": bigLineResult,
          "twoline": twoLinePaths
        };

        if (index === jsonContents.sequences[sequenceNo].length - 1) {
          fs.writeFile("../sequence" + sequenceNo + "-text-svg-path.json", JSON.stringify(q), 'utf8', function (err) {
            if (err) {
              return console.log(err);
            }
            q = [];
            console.log("The file was saved!");

          });
        }
      });
    });
  });
});

function twoLinePath(item, position) {
  let spaces = [];
  let heyGoogle = "Hey Google, ";

  for (let i = 0; i < item.length; i++) {
    if (item[i] === " ") {
      spaces.push(i);
    }
  }
  let leading = 44;

  const firstLine = heyGoogle + firstLetterLowerCase(item.slice(0, spaces[position]));
  let firstPath = textToSVG.getD(firstLine, {x: 0, y: 0, fontSize: 38.5, anchor: 'top'});

  // const secondLine = item.slice(spaces[position]+1);
  const secondLine = item.slice(spaces[position]);
  let secondPath = textToSVG.getD(secondLine, {x: 0, y: leading, fontSize: 38.5, anchor: 'top'});

  let [left,top,right,bottom] = getBounds((firstPath));
  let [l,t,r,b] = getBounds(secondPath);

  let dx = (right-r)/2;

  let centeredPath;
  let bothLines;
  let zoomLines;

  let newY = -5;
  let zoomLeading = 78;
  let newDx = dx+3;
  if(dx > 0) {
    centeredPath = textToSVG.getD(secondLine, {x: dx, y: leading, fontSize: 38.5, anchor: 'top'});

    let centeredZoomLine = textToSVG.getD(secondLine, {x: dx*(65/38.5), y: zoomLeading, fontSize: 65, anchor: 'top'});
    let zoomLine1 = textToSVG.getD(firstLine, {x: 0, y: 0, fontSize: 65, anchor: 'top'});

    zoomLines = zoomLine1+ ' ' + centeredZoomLine;
    bothLines =  (firstPath + ' ' + centeredPath);
  } else {
    centeredPath = textToSVG.getD(firstLine, {x: dx*(-1), y: 0, fontSize: 38.5, anchor: 'top'});
    let secondPath = textToSVG.getD(secondLine, {x: 0, y: leading, fontSize: 38.5, anchor: 'top'});

    let newX = dx*(-65/38.5)
    let centeredZoomLine = textToSVG.getD(firstLine, {x: dx*(-65/38.5)-5, y: newY, fontSize: 65, anchor: 'top'});
    let zoomLine2 = textToSVG.getD(secondLine, {x: -5, y: zoomLeading+newY, fontSize: 65, anchor: 'top'});

    zoomLines = centeredZoomLine + ' ' + zoomLine2;
    bothLines =  (centeredPath + ' ' + secondPath);
  }

  [left,top,right,bottom] = getBounds(bothLines);

  return {
    "bothLines": bothLines,
    "bounds": [left,right,top,bottom],
    "zoomD": zoomLines,
    "zoomBounds": getBounds(zoomLines)
  };
}



