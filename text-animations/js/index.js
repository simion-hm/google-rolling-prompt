const slowDuration = 1;

let timeline = new TimelineMax();

const textEase =  CustomEase.create("custom", "M0,0,C0.11,0.494,0.171,0.717,0.318,0.852,0.54,1.056,0.504,1,1,1");
const scrollEase = Power0.easeNone;
const growEase = Power4.easeOut;
const shrinkEase = Power4.easeOut;

const h = 86.594;
const smallFontSize = "22.6px";
const grownFontSize = "38.5px";
const grownLineHeight = "48px";
const grownCornerRadius = "27px";
const normalCornerRadius = "23px";

const promptWidth = 508;
const promptHeight = 238;

const inBetweenPromptDistance = 16;
const stepSize = h + inBetweenPromptDistance;

let tail;
let playSequence = "1-thursday-part3";
// let playSequence = "1-thursday-part1";
// let playSequence = "1-thursday-part2";
// let playSequence = "2-thursday";
// let playSequence = "2-thursday-part1";
// let playSequence = "2-thursday-part2";
// let playSequence = "3-thursday";
// let playSequence = "3-thursday-part1";
// let playSequence = "3-thursday-part2";
// let playSequence = "3-thursday-part3";
// let playSequence = "3-thursday-part4";



function loadJSON(callback) {
  let xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', 'sequences.json', false);
  xobj.onreadystatechange = function () {
    if (xobj.readyState === 4 && xobj.status === 200) {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}

const promptTextColor = "#797979";
const fadeColors = {
  text: ["#999999", "#BBBBBB", "#CCCCCC", "#E6E6E6"],
  background: ["#F7F7F7", "#FAFAFA", "#FBFBFB", "#FDFDFD",]
};

let questions = [];
const questionChildren = [];
// const questionWidth = [];
const questionID = "qid-";
const listID = "lid-";
let breakPoints;

loadJSON((response) => {
  let sequences;
  let actual_JSON = JSON.parse(response);
  sequences = actual_JSON.sequences;
  breakPoints = actual_JSON.breakPoints;
  questions = sequences[playSequence].reverse();
  console.log("List size is: " + questions.length);
  createList(questions);
  sequences = null;
  actual_JSON = null;
});

function createList(list) {
  let ul = document.getElementById("question-list");
  for (let i = 0; i < questions.length; i++) {
    let item = document.createElement('li');
    let text = document.createElement('p');

    item.setAttribute('class', 'box');
    item.setAttribute('id', listID+i);
    text.setAttribute('id', questionID + i);
    text.innerHTML = list[i];

    item.appendChild(text);
      ul.appendChild(item);
    questionChildren.push(item);
  }

  tail = questions.length;
}

let preLoaded = false;
let growIt = false;

function createAnimations(step, stepDuration, scrollingEase){
  if(step === 0){
    initializePositions();
  }

  let playGrowPair = {
    "1-thursday": "Turn on the kitchen lights",
    "1-thursday-part3": "Turn on the kitchen lights",
    "2-thursday": "Will I need an umbrella today?",
    "3-thursday": "How far away is the moon?",
    "1-thursday-part2": "Turn on the kitchen lights",
    "2-thursday-part2": "Will I need an umbrella today?",
    "3-thursday-part2": "How far away is the moon?",
    "3-thursday-part3": "How far away is the moon?",
    "3-thursday-part4": "How far away is the moon?"
  };

  let moveTime;
  const growNo = tail - (step + 5);
  const shrinkNo = tail - (step + 4);
  const labelNo = "label" + step;
  timeline.addLabel(labelNo);
  let l = timeline.getLabelsArray();
  moveTime = (step === 0)? 0:l[l.length - 1].time;

  updateAll();

  function updateAll(twoLiner = false) {
    if (
          // step === 1 ||
          // step === 0
        // ||
       growNo === 4
    ){
      twoLiner = true;
      shrink();
      grow(stepDuration, twoLiner);
    }

    // if(questions[growNo] === playGrowPair[playSequence]){
      growIt = true;
    // }

    if(step === 2){
      shrink();
    }

    if(growIt){
      grow(stepDuration, twoLiner);
      shrink();
    }

    rollAll(step, scrollEase, moveTime);
    // updateColors(step);
  }

  function initializePositions(){
    let invisibleLength = tail - 10;
    timeline.set(document.getElementById("question-list"),
        {
          top: -(invisibleLength + 1) * stepSize - 14
        }, 0);
    // timeline.set(questionChildren[invisibleLength],
    //     {
    //       color: fadeColors.background[3],
    //       backgroundColor: fadeColors.background[3]
    //     }, 0);
  }


  function rollAll(index, rollEase, rollTime) {
    if (index === 0) {
      timeline.staggerFrom("li", stepDuration,
          {
            ease: rollEase,
            y: 0,
          },
          0);
      timeline.pause();
    }
    if (tail - (8 + index)) {
      timeline.staggerTo("li", stepDuration,
          {
            ease: rollEase,
            y: stepSize * step,
            // autoRound: false,
          },
          0, rollTime);
    }
  }

  function grow(growDuration = stepDuration, twoLiner = false) {
    let growingElem = questionChildren[growNo];
    let growingText = growingElem.firstChild;

    const growWidthDuration = growDuration;
    const growTextDuration = growDuration;
    const growTextAlphaDuration = growDuration;
    const growTime = moveTime;

    const growHeightOneLiner = {
      ease: growEase,
      autoAlpha: 1,
      height: promptHeight,
      borderRadius: grownCornerRadius,
      color: promptTextColor,
      autoRound: false,
    };
    const growHeightTwoLiner = {
      ease: growEase,
      autoAlpha: 1,
      borderRadius: grownCornerRadius,
      color: promptTextColor,
      autoRound: false,
    };

    const growHeightVars = twoLiner ? growHeightOneLiner : growHeightTwoLiner;
    timeline.fromTo(growingElem, growDuration,
        {
          ease: growEase,
          lineHeight: smallFontSize,
          autoRound: false,
        },
        growHeightVars
        , growTime);
    timeline.to(growingElem,
        growTextDuration,
        {
          ease: textEase,
          fontSize: grownFontSize,
          autoRound: false,
        },
        growTime);
    timeline.to(growingText,
        growTextAlphaDuration,
        {
          ease: textEase,
          autoAlpha: 1,
          autoRound: false,
          // text: heyText(growNo),
          // delimiter: " ",
          onStart: () => {
            if(twoLiner && preLoaded) {
              const growWidthVars =
                  {
                    ease: growEase,
                    lineHeight: grownLineHeight,
                    width: promptWidth,
                    autoRound: false,
                  };
              timeline.to(questionChildren[growNo],
                  growWidthDuration - timeline.time(),
                  growWidthVars,
                  timeline.time());
              growingText.innerHTML = heyText(growNo);
            }
          },
        },
        growTime
    );
  }

  function shrink(shrinkTime = moveTime, shrinkDuration = stepDuration) {
    let shrinkingElem = questionChildren[shrinkNo];
    let shrinkText = shrinkingElem.firstChild;
    let shrinkVars =   {
          ease: shrinkEase,
          autoAlpha: 1,
          borderRadius: normalCornerRadius,
          autoRound: false,
          onStart: () => {
            timeline.set(shrinkingElem, {clearProps: "width"}, timeline.time());
            shrinkText.innerText = question(shrinkNo);
          }
    };
    let shrinkTween = new TweenMax(
        shrinkingElem, shrinkDuration,
        shrinkVars
        );
    let shrinkStepTween = new TweenMax(
        shrinkingElem, shrinkDuration,
        {
          ease: scrollingEase,
        }
    );
    let shrinkTextSizeTween = new TweenMax(
        shrinkingElem, shrinkDuration / 2,
        {
          ease: scrollingEase,
          fontSize: smallFontSize,
        }
    );

    timeline.to(shrinkingElem, shrinkDuration / 2,
        {
          ease: shrinkEase,
          height: h
        },
        shrinkTime);

    timeline.add(shrinkTween, shrinkTime);
    timeline.add(shrinkStepTween, shrinkTime);
    timeline.add(shrinkTextSizeTween, shrinkTime);
  }
  function updateColors(index) {
    for (let i = 0; i < 4; i++) {
      let tailGradient = questionChildren[tail - (index + 4 - i)];
      let headGradient = questionChildren[tail - (index + 6 + i)];
        timeline.to([headGradient,tailGradient], stepDuration,
            {
              color: fadeColors.text[i],
              backgroundColor: fadeColors.background[i],
            },
            moveTime);
    }
  }
  function question(index) {
    if(index > 0 || index < questions.length) {
      return questions[index];
    } else
      console.log("no question at index: " + index);
  }

  function heyText(index) {
    let questionText = question(index);
    let heyGoogle = "Hey Google, ";
    let text = firstLetterLowerCase(questionText);

    if (questionText.length < 23
        && !(Object.keys(breakPoints)).includes(questionText)){
      text = insertBreak(text,2,true)
    }

    let breaking = (Object.keys(breakPoints)).includes(questionText);
    if(breaking)
      return heyGoogle + insertBreak(text,breakPoints[questionText],false);
    return heyGoogle + text;
  }

  function insertBreak(text, position, end){
    const br = "<br>";
    let spaces = [];
    for (let i = 0; i < text.length; i++) {
      if (text[i] === " ") {
        spaces.push(i);
      }
    }

    if(spaces.length < 2) {
      return br + text;
    }

    if(end === true)
      text = text.substr(0, spaces[spaces.length - position]) + br + text.substr(spaces[spaces.length - position]);
    else
      text = text.substr(0, spaces[position]) + br + text.substr(spaces[position]);
    return text;
  }

  function firstLetterLowerCase(text){
    let firstLetter = text.charAt(0).toLowerCase();
    return firstLetter + text.slice(1);
  }

  if (growNo === 4){
    return moveTime;
  }
}

function playOnClick() {
  // let stopPoint = tail - 16;
  let stopPoint = tail - 8;
  let finalTime;

  timeline.smoothChildTiming = true;

  for (let i = 0; i < stopPoint; i++) {
    finalTime = createAnimations(i, slowDuration, scrollEase);
  }
  let play = false;

  // timeline.progress(1).progress(0).progress(0.81);
  timeline.progress(1).progress(0);

  timeline.autoRemoveChildren = true;
  preLoaded = true;

  if (finalTime) {
    zoomIn(finalTime);
  }

  document.addEventListener("click", () => {
    console.log("Clicked " + play);
    if (play){
      timeline.play();
    }
    else
      timeline.pause();
    play = !play;
  });
}

playOnClick();

function zoomIn(moveTime) {
  let growNo = 4;
  let stepDuration = slowDuration;

  const zoomTime = moveTime + stepDuration * 2;
  let zoomWidth = 907-64;//remove the padding
  if (growNo === 4) {
    console.log("zooming In");
    let listBox = questionChildren[growNo];
    let zoomX = (507 - 676)-40;
    let zoomY = 520+stepSize*(tail-16)-2;

    timeline.to(listBox, stepDuration,
        {
          ease: textEase,
          background:
          "radial-gradient(" +
          "ellipse at bottom, " +
          "#FCFCFC 0%," +
          "#F7F7F7 50%," +
          "#F0F0F0 100%)",
          borderRadius: 40,
          x: zoomX,
          y: zoomY,
          width: zoomWidth,
          height: 373,
          fontSize: "65px",
          lineHeight: "78px",
          autoRound: false,
        },
        zoomTime
    );

    let topBottomScaling = 1.25;
    let yShift = inBetweenPromptDistance*2*topBottomScaling;
    for (let i = 0; i < 4; i++) {
      let repositionHead = document.getElementById(listID + (growNo - i-1));
      let repositionTail = document.getElementById(listID + (growNo + i+1));
      timeline.to([repositionHead], stepDuration,
          {
            ease: textEase,
            y: zoomY - yShift * (i+1)+16,
            autoRound: false,
          },
          zoomTime
      );
      timeline.to([repositionTail], stepDuration,
          {
            ease: textEase,
            y: zoomY + yShift * (i + 1),
            autoRound: false,
          },
          zoomTime
      );
      timeline.to([repositionHead, repositionTail], stepDuration,
          {
            ease: Power4.easeOut,

            scale: topBottomScaling,
            transformOrigin: "0% 100%",
            x: zoomX,
            autoAlpha: 0,
            autoRound: false
          },
          zoomTime
      );
    }
  }
}

