# google-rolling-prompt

### Dependencies:
* JQuery
* TweenMax with  CustomEase

### Setup:

* Put your list of questions as a `"key" : ["JSON", "array"]` into `sequences.json`. Add any questions, with the question text as key and breaking point as value(indexing starts from 0) for short questions that leave an orphan word on the second line or those that need a breaking point that differs from the default one done automatically by the browser.
* Setup the box size in the `.box` class in the style.css file. 
* setup the list positioning on the screen by setting up the `ul` attribute in `style.css`
* Set the default font-size in the `li` CSS property
* Setup the prompt height, width, margin above and below in the CSS and actualize it in the JavaScript file.
* Set the variable `playSequence` to the key associated with the sequence you want to play.

* Adjust speed by changing `stepDuration`
* Set the fadeColors array to match what you need. In this case is set to what it would do if each additional box away from the central prompt would have 20% less opacity.

* Inside `createAnimations` set the `playGrowPair` to contain the prompt it will start growing one-lines.

### About the canvas-rendering branch
There is an example of using CCapture.js with Pixi.js and TweenMax with MorphSVG to render videos in the PNG format at 60fps. 
Needs feedback about usefulness. It also does animate an svg path using the particle engine. Needs improvement to work with normal HTML text elements.
It's based on the following sources:
#### For loading SVGs within a canvas element and a Pixi texture:
https://codepen.io/osublake/pen/oWzVvb
#### For CCapture recording a Pixi animation:
https://codepen.io/adkanojia/pen/EZJvJL
