const slowDuration = 1;

const h = 86.6;
const grownCornerRadius = "27px";
const normalCornerRadius = "23px";

const promptTextColor = "#797979";
const promptWidth = 508;
const promptHeight = 238;

const inBetweenPromptDistance = 16;
const stepSize = h + inBetweenPromptDistance;

let tail;
let sequences;
// let playSequence = "2-short-monday";
let playSequence = "1-thursday";
// let playSequence = "1-16-may";
let questions;


const questionWidth = [];
scrollEase = Power0.easeNone;

const sequencesPath = "../sequences.json";

function loadJSON(path, callback) {
  let xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', path, false);
  xobj.onreadystatechange = function () {
    if (xobj.readyState === 4 && xobj.status === 200) {
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}

const fadeColors = {
  text: ["#999999", "#BBBBBB", "#CCCCCC", "#E6E6E6"],
  background: ["#F7F7F7", "#FAFAFA", "#FBFBFB", "#FDFDFD",]
};

let container = document.getElementById("listContainer");
let timeline = new TimelineMax();
timeline.smoothChildTiming = true;

const containerId = "div-";
const svgId = "svg-";
const pathId = "path-";

const svgPath = "../sequence" + playSequence + "-text-svg-path.json";
let svgJSON;
let breakPoints;
loadJSON(sequencesPath, (response) => {
  let actual_JSON = JSON.parse(response);
  sequences = actual_JSON.sequences;
  breakPoints = actual_JSON.breakPoints;

  questions = sequences[playSequence].reverse();
  loadJSON(svgPath, (response) => {
    let svg_JSON = JSON.parse(response);
    svgJSON = svg_JSON;
    createList(svg_JSON, questions);
  });
});

function createList(svgData, list) {
  for (let i = 0; i < list.length; i++) {
    let div = document.createElement('div');
    div.setAttribute('id', containerId + i);
    div.setAttribute('class', "bubblePosition");
    div.innerHTML = svgData[list[i]].svg;
    div.firstChild.setAttribute('id', svgId + i);
    div.firstChild.firstChild.setAttribute('id',pathId+i);

    container.appendChild(div);
  }

  for (let i = 0; i < list.length; i++) {
    const item = document.getElementById(containerId+i);
    let w = item.getBoundingClientRect().width;
    questionWidth.push(w);
  }
  tail = questions.length;
  console.log("length: " + tail);
}


let growIt = false;
function createAnimations(step, stepDuration) {
  const growNo = tail - (step + 5);
  const shrinkNo = tail - (step + 4);
  let l = timeline.getLabelsArray();
  let moveTime = (step === 0) ? 0 : l[l.length - 1].time;

  updateAll();
  timeline.addLabel("label" + step);

  function updateAll() {
    if (step === 0
        || step === 1
        || growNo === 4
    ){
      moveTime *=0.999;
      growTwoLine();
      shrinkTwoLines();
      timeline.set(document.getElementById("placeHolderPrompt"), {
        onStart: () => {
          let zoomPrompt = document.getElementById("placeHolderPrompt");
          zoomPrompt.innerHTML = grownText(growNo);

        }
      }, moveTime-stepDuration/3);
      // zoomIn();
    }
    else {
      if(questions[growNo] === "Turn on the kitchen lights") {
        growIt = true;
      }

      // if(growIt)
        growOneLine();

      if (step === 2) {
        moveTime *=0.999;
        shrinkTwoLines();
      }
      else
        shrinkOneLine();

    }
    rollAll();
    updateGradient();
  }

  function zoomIn() {
    let zoomWidth = 907-64;//remove the padding
    if (growNo === 4) {
      let divBox = document.getElementById(containerId + growNo);
      let svgBox = document.getElementById(svgId + growNo);
      const textPath = svgBox.firstChild;
      let zoomJson = svgJSON[questions[growNo]];

      let svgW = zoomJson.twoline.zoomBounds[2];
      let svgH = zoomJson.twoline.zoomBounds[3];

      let zoomX = (507 - 676);
      let zoomY = 520+stepSize-4;

      if(playSequence === "3-short-monday")
        zoomY = 520+stepSize*2-4; // for sequence 3

      if(playSequence === "2-short-monday")
        zoomY = 520-4; // for sequence 2

      timeline.to(divBox, stepDuration,
            {
              onStart: () => {
                // timeline.pause();
              },
              background:
              "radial-gradient(" +
              "ellipse at bottom, " +
              "#FFF 0%," +
              "#F0F0F0 100%)",
              borderRadius: 40,
              x: zoomX,
              y: zoomY,
              width: zoomWidth,
              height: 373
            },
            moveTime + stepDuration * 2
        );
      timeline.to(svgBox, stepDuration,
          {
            width: "max-content",
            attr: {
              width: svgW,
              height: svgH,
            }
          },
          moveTime + stepDuration * 2
      );

      timeline.to(textPath, stepDuration,
          {
            // scale: 1.688,
            attr: {
              transform: "translate(" + ((zoomWidth - svgW) / 2 - 5) + ",0)",
              d: zoomJson.twoline.zoomD
            }
          },
          moveTime + stepDuration * 2
      );
      for (let i = 0; i < 4; i++) {
        let repositionHead = document.getElementById(containerId + (tail - (step + 6 + i)));
        let repositionTail = document.getElementById(containerId + (tail - (step + 4 - i)));

        timeline.to([repositionHead], stepDuration,
            {
              // ease: Power2.easeOut,
              x: zoomX,
              y: zoomY,
            },
            moveTime + stepDuration * 2);
        timeline.to([repositionHead], stepDuration,
            {
              ease: Power4.easeOut,
              autoAlpha: 0
            },
            moveTime + stepDuration * 2);
        timeline.to([repositionTail], stepDuration,
            {
              x: zoomX,
              y: zoomY,
            },
            moveTime + stepDuration * 2);
        timeline.to([repositionTail], stepDuration,
            {
              ease: Power4.easeOut,
              autoAlpha: 0
            },
            moveTime + stepDuration * 2);
      }
    }
  }

  function updateGradient() {
    for (let i = 0; i < 4; i++) {
      let gradientHeadText = document.getElementById(pathId+(tail - (step + 6 + i)));
      let gradientHeadBackground = document.getElementById(containerId+(tail - (step + 6 + i)));

      let gradientTailText = document.getElementById(pathId+(tail - (step + 4 - i)));
      let gradientTailBackground = document.getElementById(containerId+(tail - (step + 4 - i)));

      timeline.to([gradientHeadText,gradientHeadBackground], stepDuration,
          {
            background: fadeColors.background[i],
            attr:{
              fill: fadeColors.text[i],
            },
          },
          moveTime);
      timeline.to([gradientTailText,gradientTailBackground], stepDuration,
          {
            background: fadeColors.background[i],
            attr:{
              fill: fadeColors.text[i],
            },
          },
          moveTime);
    }
  }

  function growTwoLine(){
    const q = questions[growNo];
    const growTextAlphaDuration = stepDuration / 30;

    let divBox = document.getElementById(containerId + growNo);
    let svgBox = document.getElementById(svgId+growNo);

    const textPath = svgBox.firstChild;

    let growJson = svgJSON[q];
    let firstLineText = growJson.twoline.bothLines ;
    let svgW = growJson.twoline.bounds[1];
    let svgH = growJson.twoline.bounds[3];

    const growTwoLineContainer = {
        autoRound: false,
        borderRadius: grownCornerRadius,
        width: promptWidth,
        height: promptHeight,
    };

    const firstLineVars= {
     attr: {
       transform:"translate("+((promptWidth-svgW)/2-5)+",0)",
       fill: promptTextColor,
       d: firstLineText
     },
    };

    const growTwoLineSvg = {
      autoRound: false,
      overflow:"visible",
      attr: {
        width: svgW,
        height: svgH
      }
    };

    const growBoxTween = new TweenMax(divBox, stepDuration, growTwoLineContainer);
    const growSvgTween = new TweenMax(svgBox, stepDuration, growTwoLineSvg);

    timeline.add(growBoxTween, moveTime);
    timeline.add(growSvgTween, moveTime);

    const scaleVars = {
      scale: 0.3,
      transformOrigin: (growNo===4) ?
          "30% 20%":
          "20% 20%",
      // autoAlpha: 0.1,
      width: promptWidth,
      filter:  "url(#blur)",
      autoRound: false,
      onComplete: () => {
        timeline.set(textPath, firstLineVars, timeline.time());
        // timeline.pause();
        timeline.to(svgBox, (moveTime+stepDuration+0.3-timeline.time()), {
          autoRound: false,
          scale: 1,
          filter: "none",
        }, timeline.time());
      }
    };

    let scaleTween = new TweenMax(svgBox, growTextAlphaDuration, scaleVars);
    timeline.add(scaleTween, moveTime);
  }

  function growOneLine() {
    const q = questions[growNo];
    const growWidthDuration = stepDuration/2;

    let growPath = svgJSON[q].oneline;
    let svgBox = document.getElementById(svgId + growNo);
    const growingText = document.getElementById(svgId + growNo).firstChild;

    const growVarsTextPathOneLine = {
      autoRound: false,
      scale:1.7035,
      attr: {
        // d: growPath.childs[0].attrs.d
      }
    };
    const growVarsBoxOneLine = {
      autoRound: false,
      onStart: ()=>{
        console.log(questions[growNo] + "-execution time difference: " + (timeline.time()-moveTime));
      },
      attr: {
        width: growPath.attrs.width,
        height: growPath.attrs.height
      }
    };


    const growBoxTween = new TweenMax(svgBox, growWidthDuration, growVarsBoxOneLine);
    const growTextTween = new TweenMax(growingText, growWidthDuration, growVarsTextPathOneLine);
    console.log("at init: " + questions[growNo] + " at moveTime" + moveTime);
    console.log("execution time difference: " + (timeline.time()-moveTime));

    timeline.add(growBoxTween, moveTime*0.999);
    timeline.add(growTextTween, moveTime*0.999);
  }
  function shrinkOneLine() {
    const q = questions[shrinkNo];
    const containerBox = document.getElementById(containerId+shrinkNo);

    let firstWidth = questionWidth[shrinkNo];

    timeline.to(containerBox, stepDuration/2, {
      autoRound: false,
      height:h,
      width: questionWidth[shrinkNo]-64,
      borderRadius: normalCornerRadius,
    },
    moveTime);

    const svgBox = document.getElementById(svgId + shrinkNo);
    const shrinkText = document.getElementById(svgId + shrinkNo).firstChild;

    let initialTextPath = svgJSON[q].initial;

    const shrinkVarsTextPathOneLine = {
      filter:"none",
      autoRound: false,
      scale: 1,
      attr: {
        transform: "translate(0,0)",
        // d: initialTextPath.childs[0].attrs.d
      }
    };
    const shrinkVarsBoxOneLine = {
      autoRound: false,
      attr: {
        width: initialTextPath.attrs.width,
        height: initialTextPath.attrs.height
      }
    };

    const shrinkBoxTween = new TweenMax(svgBox, stepDuration/2, shrinkVarsBoxOneLine);
    const shrinkTextTween = new TweenMax(shrinkText, stepDuration/2, shrinkVarsTextPathOneLine);

    timeline.add(shrinkBoxTween, moveTime);
    timeline.add(shrinkTextTween, moveTime);
  }

  function shrinkTwoLines() {
    const q = questions[shrinkNo];
    const containerBox = document.getElementById(containerId+shrinkNo);

    timeline.to(containerBox, stepDuration, {
      height: h,
      autoRound: false,
    },
    moveTime);

    timeline.to(containerBox, stepDuration/1.3, {
      width: questionWidth[shrinkNo]-64,
      borderRadius: 23,
      autoRound: false,
     },
      moveTime);

    const svgBox = document.getElementById(svgId + shrinkNo);
    const shrinkText = document.getElementById(svgId + shrinkNo).firstChild;

    let initialTextPath = svgJSON[q].initial;

    const shrinkVarsTextPathOneLine = {
      onStart: () => {
        if(step > 0) {
          timeline.set(shrinkText,
            {
              autoRound: false,
              scale: 1.5035,
              autoAlpha: 0.5,
              attr: {
                // d: svgJSON[q].initial.childs[0].attrs.d
              }
            }, timeline.time());
        }
        timeline.to(shrinkText, stepDuration/1.3, {
          scale: 1,
          overflow: "visible",
          autoAlpha:1,
          transform: "translate(0,0)",
        }, timeline.time());
      },
      autoRound: false,
      attr: {
        d: initialTextPath.childs[0].attrs.d
      }
    };
    const shrinkVarsBoxOneLine = {
      transformOrigin: "30% 90%",
      autoRound: false,
      attr: {
        width: initialTextPath.attrs.width,
        height: initialTextPath.attrs.height,
      }
    };
   timeline.to(shrinkText, stepDuration/1.3, shrinkVarsTextPathOneLine, moveTime);
   timeline.to(svgBox, stepDuration/1.2,
        shrinkVarsBoxOneLine, moveTime)
  }

  function rollAll() {
    if (step === 0)
      timeline.staggerFrom(".bubblePosition", stepDuration,
        {
          ease: scrollEase,
          y: 0,
          autoRound: false
        },
        0);
    if (tail - (8 + step)) {
      timeline.staggerTo(".bubblePosition", stepDuration,
        {
          ease: scrollEase,
          y: stepSize * step,
          autoRound: false,
        },
        0, moveTime*0.998);
        // 0, moveTime);
    }
  }
}

function initializeListPositions() {
  let invisibleLength = tail - 10;
  const listContainer = document.getElementById("listContainer");
  timeline.set(listContainer, {top: -(invisibleLength + 1) * stepSize - 7}, 0);

  for (let i = 0; i < tail; i++) {
    const svgContainer = document.getElementById(containerId + i);
    const svgPath = document.getElementById(svgId + i);

    timeline.set(svgContainer,
      {
        top: (-i) * stepSize,
        autoRound: false,
      },
      0);
    timeline.set(svgPath,
      {
        autoRound: false,
        top: "50%",
        bottom: "50%",
        transform: "translate(-50%, -50%)"
      },
      0);
  }
  timeline.clear();
}

function setStartColors() {
  let invisibleLength = tail - 10;
  timeline.set(document.getElementById(pathId+invisibleLength),
      {
        autoRound: false,
        attr:{
          fill: fadeColors.background[3],
        },
      },
      0);
  for (let i = 0; i < 4; i++) {
    //tail
    const tailElem = document.getElementById(pathId+(tail - (4 - i)));
    timeline.set(tailElem,
        {
          autoRound: false,
          attr:{
            fill: fadeColors.text[i],
          },
        },
        0);
    //head
    const headElem = document.getElementById(pathId+(tail - (6 + i)));
    timeline.set(headElem,
        {
          autoRound: false,
          attr:{
            fill: fadeColors.text[i],
          },
        },
        0);
  }
}

initializeListPositions();
setStartColors();

function playOnClick() {
  for (let i = 0; i <  tail-8; i++) {
  // for (let i = 0; i < 4; i++) {
    createAnimations(i, slowDuration);
  }
  timeline.progress(1).progress(0).pause();
  // timeline.timeScale(0.7);
  let play = false;
  document.addEventListener("click", () => {
    if (timeline.paused)
      timeline.play();
    else
      timeline.pause();
    play = !play;
  });
}

playOnClick();

function question(index) {
  if (index > 0 && index < questions.length) {
    return questions[index];
  } else
    console.log("no question at index: " + index);
}

function grownText(index) {
  let questionText = question(index);
  if ((Object.keys(breakPoints)).includes(questionText))
    return twoLineCenteredSvgText(questionText, breakPoints[questionText]);
  return questionText;
}

function twoLineCenteredSvgText(text, position) {
  let spaces = [];
  let heyGoogle = "Hey Google, ";

  for (let i = 0; i < text.length; i++) {
    if (text[i] === " ") {
      spaces.push(i);
    }
  }

  const firstLine = heyGoogle + firstLetterLowerCase(text.slice(0, spaces[position]));
  const secondLine = text.slice(spaces[position]);

  return "<text transform=\"translate(287,110)\">"
    + " <tspan id=\"svgLine1\" x=\"0\" text-anchor=\"middle\">"
    + firstLine
    + "</tspan>"
    + " <tspan id=\"svgLine2\" x=\"0\" text-anchor=\"middle\" dy=\"48\">"
    + secondLine
    + "</tspan>" +
    "</text>";
}

function firstLetterLowerCase(text) {
  let firstLetter = text.charAt(0).toLowerCase();
  return firstLetter + text.slice(1);
}


